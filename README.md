# MataMarcianos-PH
Un clon de "Space Invaders" hecho usando [Phaser](http://phaser.io/).

Este juego es mi primer juego realizado por mi cuenta, esta hecho usando tecnologías web y puede ejecutarse desde cualquier navegador moderno, que soporte [canvas](http://diveintohtml5.info/canvas.html) y [localstorage](http://diveintohtml5.info/storage.html).

El juego aunque no necesita ningún servidor web o aplicación extra para jugarlo, si necesita nodejs para poder generar el juego desde el código fuente.

Son mas que invitados a clonar el repositorio, hacer mejoras y adaptar el juego a sus necesidades, también los invitos a realizar cambios en sus repositorios en caso de error ya que no deseo ofrecer soporte en casos de errores.

### Instalación
Una vez descargado el proyecto, es necesario tener un instalado nodejs con una versión >= 0.10. Desde la carpeta raíz del proyecto se deben instalar las dependencias usando el comando

    npm install 
Para instalar las dependencias de manera global usar el parámetro -g. 

Una vez instaladas las dependencias, para generar un comprimido con todos los archivos necesarios para jugar, se debe ejecutar

    gulp archive

Luego basta con descomprimir el archivo en la carpeta de su preferencia y abrir el archivo index.html para empezar a jugar. O si se desean alojar en un servidor web solo hay que descomprimir los archivos en el directorio publico del servidor.

Para ejecutar la aplicación directamente en un servidor web y tener la capacidad de refrescar los cambios que se hagan en las fuentes automáticamente, se usa el siguiente comando

    gulp devbuild

Es importante aclarar que el proyecto usa [jshint](http://jshint.com/) así que la construcción del proyecto fallará en caso de que ese complemento detecte errores en el código fuente.

### Personalización
En el archivo src/js/main.js existen ciertas variables del juego que se pueden editar para cambiar el comportamiento del juego, **ojo estos cambios pueden dañar el juego** y todas las variables números enteros positivos. 

 - GameVars.lives Las vidas del jugador, si la nave del jugador es impactada más veces que las vidas es fin del juego
 - GameVars.aliensColumns Las cantidad de columnas de enemigos que se crearán, el número de filas es fijo
 - GameVars.cantBullets Maxima cantidad de balas el jugador que aparecerán en pantallas
 - GameVars.alienSpeed Velocidad horizontal con que se desplazan los aliens
 - GameVars.alienSpeedY Velocidad vertical con que se desplazan los aliens
 - GameVars.bulletSpeedY Velocidad con que se desplazan las balas de los aliens
 - GameVars.bulletReloadTime Tiempo en milisegundos que necesita el jugador para volver a disparar
 - GameVars.scoreHitAlien Puntuación obtenida al eliminar los aliens
 - GameVars.marginBetweenAliens Distancia entre los aliens
 - GameVars.alienBulletReloadTime Tiempo en milisegundos que usan los aliens para volver a disparar

### Agredecimientos
- A mi instructor [Jorge Palacios](https://twitter.com/pctroll) por una introducción formal al mundo del desarrollo de videojuegos.
- Fondo del Juego por Rawdanitsu [Enlace](http://opengameart.org/content/space-backgrounds-3)
- Generador de Botones Da Button Factory [Enlace](http://dabuttonfactory.com/) 
- Imagenes de Aliens y Jugador Set de imagenes: raumschiffe de Reiner “Tiles” Prokein [Enlace](http://www.reinerstilesets.de/2d-grafiken/2d-vehicles/)
- Música del Menu principal: Robson Cozendey - www.cozendey.com [Enlace](http://indiegamemusic.com/viewtrack.php?id=1951)
- Sprites de los laser: Master484 - m484games.ucoz.com [Enlace](http://opengameart.org/content/bullet-collection-1-m484)
- Sonidos de laser y explosiones:
 * "Taira Komori's Japanese Free Sound Effects": http://taira-komori.jpn.org/freesounden.html [Enlace](https://www.freesound.org/people/Taira%20Komori/sounds/215438/)
 * Sergenious [Enlace](https://www.freesound.org/people/Sergenious/sounds/55818/)
 * timgormly [Enlace](https://www.freesound.org/people/timgormly/sounds/162792/)
 * LittleRobotSoundFactory - www.littlerobotsoundfactory.com [Enlace](https://www.freesound.org/people/LittleRobotSoundFactory/sounds/270310/)

### Links de interes
[Documentación de Phaser](http://phaser.io/docs)


### Contacto
Este juego para mi fue una prueba de concepto y mi primer acercamiento a Phaser, por lo que no me considero un experto de desarrollo de videojuegos, Phaser, ni nada parecido. Pero si tienen alguna duda del código me pueden contactar por mi twitter [@XR900Geopelia](https://twitter.com/XR900Geopelia) 