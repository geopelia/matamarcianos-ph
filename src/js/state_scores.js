/**
 * Created by aquintero on 25/06/2016.
 */
/* Global gameVars*/
var GameVars = GameVars || {};
GameVars.StateListScores = {
    preload: function() {},
    create: function() {
        var textConfig = {
            font: '30px Sans',
            fill: '#ffffff',
            align: 'center'
        };
        this.subTextConfig = {
            font: '16px Sans',
            fill: '#ffffff',
            align: 'center'
        };
        var strTitleMsg = 'Score List';
        var strMessage = 'Press Enter to return to the main menu';
        var strScore = 'Score';
        var strName = 'Name';
        var strLife = 'Lifes';
        var strTime = 'Time';
        this.txtMessage = GameVars.game.add.text(GameVars.game.world.centerX - 30, 30, strTitleMsg, textConfig);
        this.txtMessage.anchor.set(0.5);
        this.txtMessageContinue = GameVars.game.add.text(GameVars.game.world.centerX - 150, GameVars.game.world.height - 40, strMessage, this.subTextConfig);
        this.txtName = GameVars.game.add.text(85, 70, strName, this.subTextConfig);
        this.txtScore = GameVars.game.add.text(215, 70, strScore, this.subTextConfig);
        this.txtLifes = GameVars.game.add.text(345, 70, strLife, this.subTextConfig);
        this.txtTime = GameVars.game.add.text(475, 70, strTime, this.subTextConfig);
        this.scoreList = [];
        /* global DBUtil */
        var dbhelper = new DBUtil();
        dbhelper.getScore(this.scoreList);
        GameVars.game.time.events.add(1500, this.drawScores, this, null);

    },
    update: function() {
        /* global Phaser */
        if (GameVars.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            GameVars.game.state.start('StateIntro');
        }
    },
    drawScores: function () {
        var arraylength = 0;
        if (this.scoreList.length > 10) {
            arraylength = 10;
        } else {
            arraylength = this.scoreList.length;
        }
        var heigth = 0;
        var element = {};
        for (var index = 0; index < arraylength; index++) {
            element = this.scoreList[index];
            heigth = 88  + 20 * index;
            GameVars.game.add.text(85, heigth, element.name, this.subTextConfig);
            GameVars.game.add.text(215, heigth, element.points, this.subTextConfig);
            GameVars.game.add.text(345, heigth, element.lifes, this.subTextConfig);
            GameVars.game.add.text(475, heigth, element.gameTime.toFixedDown(2), this.subTextConfig);
        }
    }
};

Number.prototype.toFixedDown = function(digits) {
    var re = new RegExp('(\\d+\\.\\d{' + digits + '})(\\d)');
    var m = this.toString().match(re);
    return m ? parseFloat(m[1]) : this.valueOf();
};

