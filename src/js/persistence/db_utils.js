function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4();
}
/*exported DBUtil */
function DBUtil() {
    /* global localforage */
    localforage.setDriver([localforage.INDEXEDDB, localforage.LOCALSTORAGE]);
    this.saveScore = function(name, score, duration, lifes) {
        var sessionId = guid();
        /* global Score */
        var userScore = new Score(name, score, duration, lifes);
        localforage.setItem('score' + sessionId, userScore).then(function (value) {
            console.log('Value inserted ', value);
            return true;
        }).catch(function(err) {
            // This code runs if there were any errors
            console.error(err);
            return false;
        });
    };

    this.getScore = function(scores) {
        // var scores = [];
        localforage.iterate(function(value, key) {
            if (key.includes('score') && value !== undefined) {
                scores.push(value);
            }
        }).then(function() {
            console.log('Iteration has completed' + scores.length);
            scores.sort(function(score1, score2) {
                if (score1.points === score2.points) {
                    if (score1.lifes === score2.lifes) {
                        return -1 * (score1.gameTime - score2.gameTime);
                    } else {
                        return score1.lifes - score2.lifes;
                    }
                } else {
                    return score1.points - score2.points;
                }
            }).reverse();
        }).catch(function(err) {
            // This code runs if there were any errors
            console.error(err);
            return null;
        });
    };

}
