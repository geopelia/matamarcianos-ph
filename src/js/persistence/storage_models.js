/*exported Score */
function Score(name, score, duration, lifes) {
    this.name = name;
    this.points = score;
    this.date = new Date();
    this.gameTime = duration;
    this.lifes = lifes;
    this.difficulty = 'normal';
}
