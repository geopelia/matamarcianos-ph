/**
 * Created by aquintero on 20/10/2015.
 */
/* Global gameVars*/
var GameVars = GameVars || {};
GameVars.StateOver = {
    preload: function() {
    }, create: function() {
        GameVars.game.stage.backgroundColor = '#2d2d2d';
        var txtConfig = {
            font: '50px Sans',
            fill: '#ff0000',
            align: 'center'
        };
        var subTextConfig = {
            font: '20px Sans',
            fill: '#ffffff',
            align: 'center'
        };
        var strOverMsg = 'Game Over';
        var strMessage = 'Press Enter to return to the main menu';
        var strPoints = 'Your score is: ' + GameVars.game.playerScore;
        if (GameVars.game.playerLives !== 0) {
            strOverMsg = 'Congratulations';
            txtConfig.fill = '#f8e820';
        }

        this.txtMessage = GameVars.game.add.text(0, 0, strOverMsg, txtConfig);
        this.txtMessageScore = GameVars.game.add.text(GameVars.game.world.centerX - 100, GameVars.game.world.centerY, strPoints, subTextConfig);
        this.txtMessageContinue = GameVars.game.add.text(GameVars.game.world.centerX - 200, GameVars.game.world.centerY + this.txtMessageScore.height, strMessage, subTextConfig);
        this.txtMessage.anchor.set(0.5);
        this.txtMessage.x = GameVars.game.world.centerX;
        this.txtMessage.y = GameVars.game.world.centerY - 100;
        /* global Phaser */
        GameVars.game.input.keyboard.addKeyCapture([Phaser.Keyboard.ENTER]);
    }, update: function() {
        if (GameVars.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            this.click();
        }
    }, click: function() {
        this.save();
        GameVars.game.state.start('StateIntro');
    }, save: function () {
        /* global DBUtil */
        var dbhelper = new DBUtil();
        dbhelper.saveScore('LOL', GameVars.game.playerScore, GameVars.game.gameDuration, GameVars.game.playerLives);
    }
};
