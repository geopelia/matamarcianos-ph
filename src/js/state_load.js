/**
 * Created by aquintero on 08/10/2015.
 */
/* Global gameVars*/
var GameVars = GameVars || {};
GameVars.StateLoad = {
    preload: function () {
        var imgLoadVoid = GameVars.game.add.image(0, 0, 'imgLoadVoid');
        imgLoadVoid.x = GameVars.game.world.centerX - imgLoadVoid.width / 2;
        imgLoadVoid.y = GameVars.game.world.centerY - imgLoadVoid.height / 2;
        var imgLoadFull = GameVars.game.add.image(0, 0, 'imgLoadFull');
        imgLoadFull.x = imgLoadVoid.x;
        imgLoadFull.y = imgLoadVoid.y;

        GameVars.game.load.setPreloadSprite(imgLoadFull);

        GameVars.game.load.image('imgGameBg', 'img/Background-4.png');
        GameVars.game.load.image('imgTextPlay', 'img/TxtPlay.png');
        GameVars.game.load.image('imgTextQuit', 'img/TxtQuit.png');
        GameVars.game.load.image('imgPlayer', 'img/player.png');
        GameVars.game.load.image('imgAlien1', 'img/alien1.png');
        GameVars.game.load.image('imgAlien2', 'img/alien2.png');
        GameVars.game.load.image('imgAlien3', 'img/alien3.png');
        GameVars.game.load.image('imgBullet', 'img/bullet.png');
        GameVars.game.load.image('imgAlienBullet', 'img/alien_bullet.png');
        GameVars.game.load.image('imgTopBar', 'img/bg_black.png');
        GameVars.game.load.audio('bgmMusic', 'snd/06. Cycle Wheel.ogg');
        GameVars.game.load.audio('sndBullet', 'snd/215438__taira-komori__shoot02.mp3');
        GameVars.game.load.audio('sndHitPlayer', 'snd/162792__timgormly__8-bit-explosion1.ogg');
        GameVars.game.load.audio('sndAlienBullet', 'snd/270310__littlerobotsoundfactory__explosion-04.ogg');
        GameVars.game.load.audio('sndHitAlien', 'snd/55818__sergenious__boom.ogg');
        GameVars.game.load.image('imgTextScore', 'img/TxtScore.png');

    }, create: function () {
        GameVars.game.state.start('StateIntro');

    }

};
