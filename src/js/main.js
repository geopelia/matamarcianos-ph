/* Global gameVars*/
var GameVars = GameVars || {};
window.onload = function () {
    var gameWidth = 640;
    var gameHeigth = 429;
    /* debug mode */
    GameVars.debug = false;
    /* global Phaser */
    GameVars.game = new Phaser.Game(gameWidth, gameHeigth, Phaser.AUTO, 'phaser_game');
    GameVars.game.state.add('StateBoot', GameVars.StateBoot);
    GameVars.game.state.add('StateLoad', GameVars.StateLoad);
    GameVars.game.state.add('StateIntro', GameVars.StateIntro);
    GameVars.game.state.add('StateMain', GameVars.StateMain);
    GameVars.game.state.add('StateOver', GameVars.StateOver);
    GameVars.game.state.add('StateListScores', GameVars.StateListScores);
    GameVars.game.state.start('StateBoot');
    /* My game constants */
    GameVars.lives = 3;
    GameVars.aliensColumns = 8;
    GameVars.cantBullets = 20;
    GameVars.alienSpeed = 100;
    GameVars.alienSpeedY = 250;
    GameVars.bulletSpeedY = 300;
    GameVars.bulletReloadTime = 500;
    GameVars.scoreHitAlien = 30;
    GameVars.marginBetweenAliens = 38;
    GameVars.alienBulletReloadTime = 150;
};
