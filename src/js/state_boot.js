/* Global gameVars*/
var GameVars = GameVars || {};
GameVars.StateBoot = {
    preload: function () {
        GameVars.game.load.image('imgLoadVoid', 'img/progress_void.png');
        GameVars.game.load.image('imgLoadFull', 'img/progress_full.png');

    }, create: function () {
        GameVars.game.scale.pageAlignHorizontally = true;
        if (!GameVars.game.device.desktop) {
            /* global Phaser */
            GameVars.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        }
        GameVars.game.scale.refresh();
        GameVars.game.state.start('StateLoad');
    }


};
