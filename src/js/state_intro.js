/**
 * Created by aquintero on 08/10/2015.
 */

/* Global gameVars*/
var GameVars = GameVars || {};
GameVars.StateIntro = {
    preload: function() {

    }, create: function () {
        GameVars.game.stage.backgroundColor = '#2d2d2d';
        var titleConfig = {
            font: '50px Sans',
            fill: '#ffffff',
            align: 'center'
        };
        var strTitle = 'Space Invaders';
        var txtTitle = GameVars.game.add.text(0, 0, strTitle, titleConfig);
        GameVars.bgmMusic = GameVars.game.add.audio('bgmMusic');
        GameVars.bgmMusic.loop = true;
        GameVars.bgmMusic.play();
        txtTitle.anchor.set(0.5);
        txtTitle.x = GameVars.game.world.centerX;
        txtTitle.y = GameVars.game.world.centerY - 100;

        var btnPlay = GameVars.game.add.button(GameVars.game.world.centerX, GameVars.game.world.centerY - 30, 'imgTextPlay', this.clickButton);
        btnPlay.anchor.set(0.5);
        var btnQuit = GameVars.game.add.button(GameVars.game.world.centerX, GameVars.game.world.centerY + 30, 'imgTextQuit', this.clickQuit);
        btnQuit.anchor.set(0.5);
        var btnScore = GameVars.game.add.button(GameVars.game.world.centerX, GameVars.game.world.centerY + 90, 'imgTextScore', this.clickScore);
        btnScore.anchor.set(0.5);
    }, clickButton: function () {
        GameVars.game.state.start('StateMain');
    }, clickQuit: function() {
        GameVars.game.destroy();
    }, clickScore: function() {
        GameVars.game.state.start('StateListScores');
    }
};
