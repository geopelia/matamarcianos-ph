/**
 * Created by aquintero on 08/10/2015.
 */
/* Global gameVars*/
var GameVars = GameVars || {};
GameVars.StateMain = {
    preload: function () {

    }, create: function () {
        /* global Phaser */
        GameVars.game.physics.startSystem(Phaser.Physics.ARCADE);
        GameVars.game.physics.arcade.checkCollision.down = false;

        this.score = 0;
        this.lives = GameVars.lives;
        this.bulletTime = 0;
        this.alienBulletTime = 0;
        this.blackBarHeight = 24;

        var txtConfig = {
            font: '22px Sans',
            fill: '#fff',
            align: 'center'
        };

        this.sndBullet = GameVars.game.add.audio('sndBullet');
        this.sndHitPlayer = GameVars.game.add.audio('sndHitPlayer');
        this.sndHitAlien = GameVars.game.add.audio('sndHitAlien');
        this.sndAlienBullet = GameVars.game.add.audio('sndAlienBullet');
        this.sndAlienBullet.volume = 0.6;

        var imgBg = GameVars.game.add.sprite(0, 0, 'imgGameBg');
        imgBg.anchor.set(0.5);
        imgBg.x = GameVars.game.world.centerX;
        imgBg.y = GameVars.game.world.centerY;

        this.imgPlayer = GameVars.game.add.sprite(0, 0, 'imgPlayer');
        GameVars.game.physics.arcade.enable(this.imgPlayer);
        this.imgPlayer.body.enable = true;
        this.imgPlayer.body.immovable = true;
        this.imgPlayer.anchor.set(0.5, 1);
        this.imgPlayer.speed = 40;
        this.imgPlayer.scale.set(0.6);
        this.imgPlayer.half = this.imgPlayer.width / 2;

        this.aliens = GameVars.game.add.group();
        this.aliens.enableBody = true;
        this.aliens.physicsBodyType = Phaser.Physics.ARCADE;
        var numCols = GameVars.aliensColumns;
        var alienList = ['imgAlien3', 'imgAlien1', 'imgAlien2'];
        for (var i = 0; i < alienList.length; i++) {
            var imgAlien = alienList[i];
            for (var j = 0; j < numCols; j++) {
                var al = this.aliens.create(0, 0, imgAlien);
                al.x = (j * al.width) + 10;
                al.y = (i * al.height) + this.blackBarHeight;
                al.first = true;
            }
        }
        this.bullets = GameVars.game.add.group();
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.enableBody = true;
        for (var l = 0; l < GameVars.cantBullets; l++) {
            var b = this.bullets.create(0, 0, 'imgBullet');
            b.exists = false;
            b.visible = false;
            b.checkWorldBounds = true;
            b.events.onOutOfBounds.add(this.resetBullet, this);
        }
        this.aliensBullets = GameVars.game.add.group();
        this.aliensBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.aliensBullets.enableBody = true;
        for (var k = 0; k < GameVars.cantBullets; k++) {
            var ba = this.aliensBullets.create(0, 0, 'imgAlienBullet');
            ba.exists = false;
            ba.visible = false;
            ba.checkWorldBounds = true;
            ba.events.onOutOfBounds.add(this.resetBullet, this);
        }

        this.aliens.setAll('body.velocity.x', GameVars.alienSpeed, true, true, 0);
        this.aliens.setAll('body.collideWorldBounds', true, true, true, 0);
        this.aliens.forEach(this.makeBounce, this, true);
        this.prevPosX = GameVars.game.input.x;
        GameVars.game.input.onDown.add(this.fireBullet, this);
        GameVars.game.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);

        this.imgBgBlack = GameVars.game.add.tileSprite(0, 0, GameVars.game.world.width, this.blackBarHeight, 'imgTopBar');
        var strScore = 'Score: ' + this.score;
        this.txtScore = GameVars.game.add.text(0, 0, strScore, txtConfig);

        var strLives = 'Lives: ' + this.lives;
        this.txtLives = GameVars.game.add.text(0, 0, strLives, txtConfig);
        this.txtLives.anchor.set(1, 0);
        this.txtLives.x = GameVars.game.world.width;
        this.txtLives.align = 'right';
        this.reset();

        this.beginGameTime = Date.now();

    }, update: function () {
        GameVars.game.physics.arcade.overlap(this.aliens, this.bullets, this.hitAlien, null, this);
        GameVars.game.physics.arcade.overlap(this.imgPlayer, this.aliens, this.crashPlayer, null, this);
        GameVars.game.physics.arcade.overlap(this.imgPlayer, this.aliensBullets, this.hitPlayer, null, this);
        GameVars.game.physics.arcade.overlap(this.bullets, this.aliensBullets, this.annulBullets, null, this);

        var keyLeft = GameVars.game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        var keyRight = GameVars.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT);
        var velocityX = 0;
        if (keyLeft) {
            velocityX = -1;
        } else if (keyRight) {
            velocityX = 1;
        }

        var deltaTime = GameVars.game.time.physicsElapsed;
        var posX = GameVars.game.input.x;

        if (velocityX !== 0) {
            this.imgPlayer.x += this.imgPlayer.speed * velocityX * deltaTime;
        } else if (this.prevPosX !== posX) {
            this.imgPlayer.x = posX;
        }

        this.prevPosX = posX;
        if (this.imgPlayer.x + this.imgPlayer.half > GameVars.game.world.width) {
            this.imgPlayer.x = GameVars.game.world.width - this.imgPlayer.half;
        } else if (this.imgPlayer.x < this.imgPlayer.half) {
            this.imgPlayer.x = this.imgPlayer.half;
        }
        this.aliens.forEach(this.makeAdvanceY, this, true);

        if (GameVars.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            this.fireBullet();
        }
        var alien = this.aliens.getRandom();
        if (this.aliens.checkProperty(alien, 'alive', true)) {
            this.fireAlien(alien);
        }
    }, reset: function () {
        this.imgPlayer.x = GameVars.game.world.centerX;
        this.imgPlayer.y = GameVars.game.world.height;
    }, makeBounce: function(alien) {
        alien.body.bounce.set(1);
    }, makeAdvanceY: function (alien) {
        var deltaTime = GameVars.game.time.physicsElapsed;
        if (alien.x >= (GameVars.game.world.width - alien.width)) {
            alien.y += GameVars.alienSpeedY * deltaTime;
        } else if (alien.x === 0 && !alien.first) {
            alien.y += GameVars.alienSpeedY * deltaTime;
        }
        if (alien.y >= (GameVars.game.world.height - alien.height)) {
            this.gameOver();
        }
        if (alien.first) {
            alien.first = false;
        }
    }, resetBullet: function (bullet) {
        bullet.kill();
    }, fireBullet: function () {
        if (GameVars.game.time.now > this.bulletTime) {
            var bullet = this.bullets.getFirstExists(false);
            if (bullet) {
                this.sndBullet.play();
                bullet.reset(this.imgPlayer.x - 4, this.imgPlayer.y - this.imgPlayer.height);
                bullet.body.velocity.y = -GameVars.bulletSpeedY;
                this.bulletTime = GameVars.game.time.now + GameVars.bulletReloadTime;
            }
        }
    }, hitAlien: function (alien, bullet) {
        this.sndHitAlien.play();
        alien.kill();
        bullet.kill();
        this.score += GameVars.scoreHitAlien;
        this.txtScore.text = 'Score: ' + this.score;
        if (this.aliens.countLiving() === 0) {
            this.gameOver();
        }
    }, crashPlayer: function(player, alien) {
        this.sndHitPlayer.play();
        alien.kill();
        this.lives--;
        this.txtLives.text = 'Lives: ' + this.lives;
        if (this.lives === 0) {
            this.gameOver();
        }
    }, fireAlien: function(alien) {
        var canShoot = false;
        var alienMidLenght = 0;
        var anotherAlienLenght = 0;
        if (this.aliens.countLiving() > 1) {
            for (var i = 0; i < this.aliens.length; i++) {
                var anotherAlien = this.aliens.getAt(i);
                if (alien !== anotherAlien && this.aliens.checkProperty(anotherAlien, 'alive', true)) {
                    if (alien.y >= anotherAlien.y) {
                        canShoot = true;
                    } else {
                        alienMidLenght = alien.x + alien.body.halfWidth;
                        anotherAlienLenght = anotherAlien.x + anotherAlien.width;
                        if (alienMidLenght - GameVars.marginBetweenAliens > anotherAlienLenght ||
                            alienMidLenght + GameVars.marginBetweenAliens < anotherAlien.x) {
                            canShoot = true;
                        } else {
                            canShoot = false;
                            break;
                        }
                    }
                }
            }
        } else {
            canShoot = true;
        }
        if (GameVars.game.time.now > this.alienBulletTime && canShoot) {
            this.debugKg = alien;
            var alienBullet = this.aliensBullets.getFirstExists(false);
            if (alienBullet) {
                this.sndAlienBullet.play();
                alienBullet.reset(alien.x + alien.body.halfWidth, alien.y + alien.height);
                alienBullet.body.velocity.y = GameVars.bulletSpeedY;
                this.alienBulletTime = GameVars.game.time.now + GameVars.alienBulletReloadTime;
            }
        }
    }, hitPlayer: function (player, allienBullet) {
        this.sndHitPlayer.play();
        allienBullet.kill();
        this.lives--;
        this.txtLives.text = 'Lives: ' + this.lives;
        if (this.lives === 0) {
            this.gameOver();
        }
    }, annulBullets: function(bullet, alienBullet) {
        bullet.kill();
        alienBullet.kill();
    }, gameOver: function () {
        GameVars.bgmMusic.stop();
        GameVars.game.gameDuration = GameVars.game.time.elapsedSecondsSince(this.beginGameTime);
        GameVars.game.playerLives = this.lives;
        GameVars.game.playerScore = this.score;
        GameVars.game.state.start('StateOver');
    }, render: function () {
        if (this.debugKg instanceof Phaser.Sprite && GameVars.debug) {
            this.game.debug.spriteBounds(this.debugKg);
        }
    }
};
